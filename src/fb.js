import firebase from 'firebase/app'
import 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyBM-kCwqtCOSfDhdllqmt9bhAnoao4Pe6g",
  authDomain: "bootfi-news.firebaseapp.com",
  databaseURL: "https://bootfi-news.firebaseio.com",
  projectId: "bootfi-news",
  storageBucket: "bootfi-news.appspot.com",
  messagingSenderId: "279935701895",
  appId: "1:279935701895:web:632d6587e92d5777c7b922",
  measurementId: "G-BWPJ7PY1TJ"
}
firebase.initializeApp(firebaseConfig)

const db = firebase.firestore()

db.settings({ timestampsInSnapshots: true})

export default db
