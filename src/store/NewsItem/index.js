import * as firebase from 'firebase'

export default{
  state: {
    loadedNewsItems: [
      { title: 'what is a Vue.js',
        id: '1mvcgvhvjm',
        date: new Date(),
        auther: 'al7omodd',
        location: 'KSA',
        description: 'Vue.js is an amazing framwork for built your website easyly',
        imageUrl: 'https://modernweb.com/wp-content/uploads/2017/03/vue.jpg'},
      { title: 'Apply to Bootfi Jobs',
        id: '2cgncgncghcghj',
        date: new Date(),
        auther: 'al7omodd',
        location: 'Aden',
        description: 'welcome to bootfi company andsend your CV',
        imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTGqbdSsCiBb6hAocWXj6xDZxY-sBm8TMhGvX3q_5ANEFGYJ77b&usqp=CAU'},
      { title: 'Loin in Nuxt.js cource',
        id: 'jvhkvhvhk3',
        date: new Date(),
        auther: 'al7omodd',
        location: 'Yemen',
        description: 'Nuxt.js is an amazing framwork',
        imageUrl: 'https://www.coderomeos.org/storage/uploads/images/posts/nuxt-universal-vuejs-applications-5a8e7308f2f96.png'}
    ]
  },
  mutations: {
    setLoadedNewsItems (state, payload) {
      state.loadedNewsItems = payload
    },
    createNewsItem (state, payload) {
      state.loadedNewsItems.push(payload)
    },
    updateNewsItem (state, payload) {
      const newsItem = state.loadedNewsItems.find(newsItem => {
        return newsItem.id === payload.id
      })
      if (payload.title) {
        newsItem.title = payload.title
      }
      if (payload.description) {
        newsItem.description = payload.description
      }
      if (payload.date) {
        newsItem.date = payload.date
      }
    }
  },
  actions: {
    loadNewsItems ({commit}) {
      commit('setLoading', true)
      firebase.database().ref('newsItems').once('value')
      .then((data) => {
        const newsItems = []
        const obj = data.val()
        for (let key in obj) {
          newsItems.push({
            id: key,
            title: obj[key].title,
            description: obj[key].description,
            imageUrl: obj[key].imageUrl,
            date: obj[key].data,
            location: obj[key].location,
            autherId: obj[key].autherId
          })
        }
        commit('setLoadedNewsItems', newsItems)
        commit('setLoading', false)
      })
      .catch(
        (error) => {
          console.log(error)
          commit('setLoading', true)
        }
      )
    },
    createNewsItem ({commit, getters}, payload) {
      const newsItem = {
        title: payload.title,
        location: payload.location,
        // imageUrl: payload.imageUrl,
        description: payload.description,
        date: payload.date.toString(),
        id: getters.user.id
      }
      // Reach out to firebase and store it
      // let imageUrl
      // let key
      console.log('News Items : ', newsItem)
      firebase.database().ref('newsItems').push(newsItem)
      .then(() => {
        console.log('uuuu')
      }
      )
      // .then((data) => {
      //   key = data.key
      //   return key
      // })
      // .then(key => {
      //   const filename = payload.image.name
      //   const ext = filename.slice(filename.lastIndexOf('.'))
      //   return firebase.storage().ref('newsItems/' + key + '.' + ext).put(payload.image)
      // })
      // .then(fileData => {
      //   imageUrl = fileData.getMetadata.downloadURLs[0]
      //   return firebase.database().ref('newsItems').child(key).update({imageUrl: imageUrl})
      // })
      // .then(() => {
      //   commit('createNewsItem', {
      //     ...newsItem,
      //     imageUrl: imageUrl,
      //     id: key
      //   })
      // })
      // .catch((error) => {
      //   console.log(error)
      // })
    },
    updateNewsItemData ({commit}, payload) {
      commit('setLoading', true)
      const updateobj = {}
      if (payload.title) {
        updateobj.title = payload.title
      }
      if (payload.description) {
        updateobj.description = payload.description
      }
      if (payload.date) {
        updateobj.date = payload.date
      }
      firebase.database().ref('newsItem').child(payload.id).update(updateobj)
      .then(() => {
        commit('setLoading', false)
        commit('updateNewsItem', payload)
      })
      .catch(error => {
        console.log(error)
        commit('setLoading', false)
      })
    }
  },
  getters: {
    loadedNewsItems (state) {
      return state.loadedNewsItems.sort((newsitemA, newsitemB) => {
        return newsitemA.date > newsitemB.date
      }
      )
    },
    featuredNewsItems (state, getters) {
      return getters.loadedNewsItems.slice(0, 50)
    },
    loadedNewsItem (state) {
      return (newsItemId) => {
        return state.loadedNewsItems.find((newsItem) => {
          return newsItem.id === newsItemId
        }
        )
      }
    }
  }
}
